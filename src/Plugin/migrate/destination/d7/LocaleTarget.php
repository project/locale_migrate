<?php

namespace Drupal\locale_migrate\Plugin\migrate\destination\d7;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class imports one locale target.
 *
 * @MigrateDestination(
 *   id = "d7_locale_target"
 * )
 */
class LocaleTarget extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a locale target plugin.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The current migration.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->connection = $connection;
    $this->supportsRollback = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $lid = $row->getDestinationProperty('lid');
    $translation = $row->getDestinationProperty('translation');
    $language = $row->getDestinationProperty('language');

    if (empty($old_destination_id_values[0]) && empty($old_destination_id_values[1])) {
      $this->connection
        ->insert('locales_target')
        ->fields([
          'lid' => $lid,
          'translation' => $translation,
          'language' => $language,
        ])
        ->execute();
    }
    else {
      $this->connection
        ->update('locales_target')
        ->fields([
          'translation' => $translation,
        ])
        ->condition('lid', $lid)
        ->condition('language', $language)
        ->execute();
    }

    return [
      'lid' => $lid,
      'language' => $language,
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    $this->connection
      ->delete('locales_target')
      ->condition('lid', $destination_identifier, 'IN')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'lid' => [
        'type' => 'integer',
      ],
      'language' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'lid' => $this->t('Source string ID.'),
      'translation' => $this->t('Translation string value in the language.'),
      'language' => $this->t('Language code.'),
    ];
  }

}
