<?php

namespace Drupal\locale_migrate\Plugin\migrate\destination\d7;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class imports one locale source.
 *
 * @MigrateDestination(
 *   id = "d7_locale_source"
 * )
 */
class LocaleSource extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a locale source plugin.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The current migration.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->connection = $connection;
    $this->supportsRollback = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $lid = $row->getDestinationProperty('lid');
    $source = $row->getDestinationProperty('source');
    $context = $row->getDestinationProperty('context');
    $version = $row->getDestinationProperty('version');

    if (empty($old_destination_id_values)) {
      $newLid = $this->connection
        ->insert('locales_source')
        ->fields([
          'lid' => $lid,
          'source' => $source,
          'context' => $context,
          'version' => $version,
        ])
        ->execute();
    }
    else {
      $newLid = $old_destination_id_values[0];

      $this->connection
        ->update('locales_source')
        ->fields([
          'source' => $source,
          'context' => $context,
          'version' => $version,
        ])
        ->condition('lid', $newLid)
        ->execute();
    }

    return [$newLid];
  }


  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    $this->connection
      ->delete('locales_source')
      ->condition('lid', $destination_identifier, 'IN')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'lid' => [
        'type' => 'integer',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'lid' => $this->t('Unique identifier of the string.'),
      'source' => $this->t('The original string in English.'),
      'context' => $this->t('The context the string applies to.'),
      'version' => $this->t('Version of Drupal, where the string was last used (for locales optimization).'),
    ];
  }

}
