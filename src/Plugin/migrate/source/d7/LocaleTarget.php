<?php

namespace Drupal\locale_migrate\Plugin\migrate\source\d7;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 locale target.
 *
 * @MigrateSource(
 *   id = "d7_locale_target",
 *   source_module = "locale",
 *   destination_module = "locale"
 * )
 */
class LocaleTarget extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('locales_target', 'l');
    $query->join('locales_source', 'ls', 'l.lid = ls.lid');
    $query->condition('ls.textgroup', 'default');
    $query->fields('l');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'lid' => [
        'type' => 'integer',
      ],
      'language' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'lid' => $this->t('Source string ID.'),
      'translation' => $this->t('Translation string value in the language.'),
      'language' => $this->t('Language code.'),
    ];
  }

}
