<?php

namespace Drupal\locale_migrate\Plugin\migrate\source\d7;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 locale source.
 *
 * @MigrateSource(
 *   id = "d7_locale_source",
 *   source_module = "locale",
 *   destination_module = "locale"
 * )
 */
class LocaleSource extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('locales_source', 'l');
    $query->join('locales_target', 'lt', 'l.lid = lt.lid');
    $query->condition('textgroup', 'default');
    $query->fields('l');
    $query->distinct();

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'lid' => [
        'type' => 'integer',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'lid' => $this->t('Unique identifier of the string.'),
      'source' => $this->t('The original string in English.'),
      'context' => $this->t('The context the string applies to.'),
      'version' => $this->t('Version of Drupal, where the string was last used (for locales optimization).'),
    ];
  }

}
